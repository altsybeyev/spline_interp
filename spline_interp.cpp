#include "spline_interp.hpp"

arma::vec cubic_spline(const arma::vec& x, const arma::vec& y, const arma::vec& xx)
{
    auto n = x.n_elem;
    if (n != y.n_elem)
    {
        throw std::invalid_argument("non-equal sizes of x and y");
    }
    auto h = arma::vec(x.rows(1, n - 1) - x.rows(0, n - 2));
    auto d = arma::vec((y.rows(1, n - 1) - y.rows(0, n - 2)) / h);

    auto lower = arma::vec(h.rows(0, h.n_rows - 3));
    auto main  = arma::vec(2 * (h.rows(0, h.n_rows - 2) + h.rows(1, h.n_rows - 1)));
    auto upper = arma::vec(h.rows(2, h.n_rows - 1));

    arma::mat T;
    T.zeros(n - 2, n - 2);

    for (size_t i = 0; i < n - 2; i++)
    {
        T(i, i) = main(i);
    }
    for (size_t i = 0; i < n - 3; i++)
    {
        T(i, i + 1) = upper(i);
    }
    for (size_t i = 0; i < n - 3; i++)
    {
        T(i + 1, i) = lower(i);
    }

    auto rhs = 6 * (d.rows(1, d.n_rows - 1) - d.rows(0, d.n_rows - 2));

    auto m = arma::vec(arma::solve(T, rhs));
    m.insert_rows(0, 1);
    m.insert_rows(m.n_rows, 1);

    auto s0 = y;
    auto s1 = arma::vec(d - h % (2 * m.rows(0, m.n_rows - 2) + m.rows(1, m.n_rows - 1)) / 6);
    auto s2 = arma::vec(m / 2);
    auto s3 = arma::vec((m.rows(1, m.n_rows - 1) - m.rows(0, m.n_rows - 2)) / (6 * h));

    arma::vec result(xx.n_elem);

    for (size_t i = 0; i < xx.n_elem; i++)
    {
        auto xx_v = xx(i);

        auto xind = x.n_elem - 2;

        if (xx_v != x(x.n_elem - 1))
        {
            xind = std::distance(x.begin(), std::upper_bound(x.begin(), x.end(), xx_v)) - 1;
        }
        if (i == xx.n_elem - 1)
        {
            int tt = 0;
        }
        auto xi = x(xind);

        result(i) = s0(xind) + s1(xind) * (xx_v - xi) + s2(xind) * std::pow(xx_v - xi, 2) +
                    s3(xind) * std::pow(xx_v - xi, 3);
    }
    return result;
}

arma::vec get_arr(const size_t i, const size_t len)
{
    size_t    ni = 3;
    arma::vec result(1);
    result(0) = i;

    for (size_t k = 1; k <= ni; k++)
    {
        auto ind = i - k;
        if (ind >= 0)
        {
            result.insert_rows(0, 1);
            result(0) = ind;
        }
    }
    for (size_t k = 1; k <= ni; k++)
    {
        auto ind = i + k;
        if (ind <= len)
        {
            result.insert_rows(result.n_rows, 1);
            result(result.n_rows - 1) = ind;
        }
    }

    return result;
}

arma::vec cubic_spline3d(const arma::vec& x, const arma::vec& y, const arma::vec& z,
                         const arma::cube& data, const arma::vec& xx, const arma::vec& yy,
                         const arma::vec& zz)
{
    for (size_t i = 0; i < xx.n_elem; i++)
    {
        const auto x_interp = xx(i);
        const auto y_interp = yy(i);
        const auto z_interp = zz(i);

        auto dz = arma::vec(arma::abs(z - z_interp));

        auto z_i = std::distance(dz.begin(), std::min_element(dz.begin(), dz.end()));

        auto z_arr = get_arr(z_i, zz.n_elem);
    }
}

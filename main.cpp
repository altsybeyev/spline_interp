#include "spline_interp.hpp"

arma::vec runge(const arma::vec& x)
{
    arma::vec tmp;
    tmp.ones(x.n_elem);
    return tmp / (1 + 25 * x % x);
}

int main()
{
    auto x  = arma::linspace(-1, 1, 20);
    auto y  = runge(x);
    auto xx = arma::linspace(-1, 1, 1000);
    auto yy = cubic_spline(x, y, xx);

    return 0;
}
#include <armadillo>

arma::vec cubic_spline(const arma::vec& x, const arma::vec& y, const arma::vec& xx);

arma::vec cubic_spline3d(const arma::vec& x, const arma::vec& y, const arma::vec& z,
                         const arma::cube& data, const arma::vec& xx, const arma::vec& yy,
                         const arma::vec& zz);
